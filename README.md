Ansible Proxmox Configuration Role
=========

An Ansible role to install and configure components required for proxmox
systems.

Requirements
------------

None.

Role Variables
--------------

None.

Dependencies
------------

None.

Example Playbook
----------------

User the role as follows:

```yaml
  - hosts: servers
    roles:
      - role: proxmox_config
```

License
-------

AGPLv3

Author Information
------------------

Rob Connolly [https://webworxshop.com](https://webworxshop.com)
